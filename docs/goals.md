# *Goals for Development* #

## *Have a game with map loading* ##

	For this project we would like to develope a reliable and neat way of loading into different scenes or "levels"
	
	Some things we would like to impliment in these levels are :
		
		Object Perminance - Having objects that stay with the player controller as they travel from level to level
		World Perminance - Having objects that are dropped in a map stay with the map. 							
		An example would be if we brought a sword to a map and dropped it,
		that sword should stay at the position it was dropped unless someone else picks it up.

## *Have reliable networking* ##

	Using the photon networking asset, we would like to have a game where object states, character states, and world states
	are translated well throughout the internet such that it is passable and not bringing our game down.

	Some notes on how to do this :

		Read up on the photon networking docs
		Watch some video tutorials
		Consistently work on this feature as new code gets pushed to a stable branch

## *Have a melee/shooting system* ##

	Being an RPG, it is no fun just to walk around. We would like to develope a game where we can effectively add
	a competitive nature to the game via a melee/shooting system. This would be for hand weapons and spells/staffs.

	Possible Roadblocks to keep in mind :

		Networking needs to record hits accurately and not take away from the user experience.
		Raycast physics vs a more reliable approach
		Reflection in animations.

## *Good Documentation* ##

	With any coding assignment, a developer needs to grow comfortable with standard practices in development.

	This includes:

		Efficient code commenting
		Documenting features for other developers
		Index of controls
		Effective use of git and version control practices (See docs/development.md)


## *Lore* ##

	We do not need the next big story driven game but we should have some lore and possibly a quest system.
	Including NPCs would be cool as well.

## *Grow a love for programming* ##

	New members exploring the field of game development should work hard, but still have fun.
	If game developement is not something you can motivate yourself to do it might not be your thing.
	At the end of the day, learning and gaining passion and drive are the best tools of any professional.


