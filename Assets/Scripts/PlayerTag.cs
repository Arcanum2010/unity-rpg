﻿using UnityEngine;
using System.Collections;

public class PlayerTag : MonoBehaviour {

	void Start () {
		transform.position = transform.parent.position + new Vector3(0,1.5f,0);
	}

	void Update() {
		GetComponent<TextMesh> ().text = transform.parent.name;
		GetComponent<TextMesh> ().color = transform.parent.GetComponentInChildren<SkinnedMeshRenderer>().material.color;
		//This may need to be changed to an rpc somewhere else
	}
}
