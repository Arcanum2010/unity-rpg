﻿using UnityEngine;
using System.Collections;

public class ObjectiveObject : MonoBehaviour {
	public Action action;
	[Tooltip("Memorable name of objective")]
	public string ObjectiveName; 
	[Tooltip("Description of the objective (Create Only)")]
	public string Description;
	[Tooltip("Pairs of memorable names and descriptions for creating multi-point objectives (Create Only)")]
	public string[] SubObjectives;
	private GameObject go;
	private ObjectivesList objectives;

	/**
	 * Use this component to create and complete your objectives/quests in game
	 * not really important to know how this works, but know how to use it
	 * all quest logic contained in ObjectivesList.cs
	 */
	void Start(){
		go = GameObject.Find ("Objectives List");
		objectives = go.GetComponent<ObjectivesList>(); // automatically attach itself to the main list of quests
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.tag.Equals ("Player")) {
			Debug.Log (other.name + " entered " + gameObject.name);
			switch (action) {
			case Action.create:
				other.GetComponent<ObjectivesListener>().addSimpleObjective(ObjectiveName, Description);
				break;
			case Action.createComplex:
				other.GetComponent<ObjectivesListener>().addComplexObjective(ObjectiveName, Description, SubObjectives);
				break;
			case Action.complete:
				other.GetComponent<ObjectivesListener>().completeObjective(ObjectiveName);
				break;
			}
		}
	}
	
	void OnTriggerStay(Collider other) {
		if (other.tag.Equals ("Player")) {
			Debug.Log ("Player in Triggerzone");
		}
	}
	void OnTriggerExit(Collider other) {
		if (other.tag.Equals ("Player")) {
			Debug.Log ("Player exited Triggerzone");
		}
	}
}

public enum Action // your custom enumeration
{
	create, 
	createComplex, 
	complete
};
