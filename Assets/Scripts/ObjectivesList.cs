﻿using UnityEngine;
using System.Collections;

public class ObjectivesList : MonoBehaviour {
	public ArrayList objectives = new ArrayList(); // master list of all quest points
	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	//	transform.position = new Vector3 (0, 1, 0);
//		transform.rotation = new Quaternion (0, 0, 0, 0);
	}
	
	// Called each time an objective is added or removed
	void updateText(){

		string text = "OBJECTIVES:\n";												// display always has this header
		foreach (Objective objective in objectives) {							
			if (!objective.completed) {												// don't display completed quests
				if (objective.GetType ().Equals (typeof(MultiObjective))) {			// if it's a multi point objective call it as such
					text += "::" + ((MultiObjective)objective).getDisplay () + "\n";// add the quest's text and a newline
				} else
					text += "::" + objective.getDisplay () + "\n";					// add the quest's text and a newline
			}
		}
		GetComponent<TextMesh>().text = text;	// actually update the the text in the gameObject
	}
	
	public void addSimpleObjective(string shortName, string displayText){
		foreach (Objective obj in objectives) 	// protect against duplicate objectives
			if ( obj.shortName.Equals(shortName) )
				return;
		
		objectives.Add (new Objective (shortName, displayText));
		updateText ();				// always update the text after you update the objective list
	}
	
	public void addComplexObjective(string shortName, string displayText, string[] bullets){
		foreach (Objective obj in objectives)	// protect against duplicate objectives
			if ( obj.shortName.Equals(shortName) )
				return;
		objectives.Add (new MultiObjective (shortName, displayText, bullets));
		updateText ();				// always update the text after you update the objective list
	}
	
	public void completeObjective(string name){
		foreach(Objective item in objectives){
			if(item.shortName.Equals(name))	
				item.completeObjective();
			if(item.GetType().Equals(typeof(MultiObjective)))
				((MultiObjective) item).completeSubObjective(name);	// check if name is from sub objective
		}
		updateText ();			// always update the text after you update the objective list
	}
}

// base type of objective
class Objective {
	public string shortName;		// a short, memorable name to quickly reference the objective
	protected string displayText;	// the long form of the quest, what is actually shown to the player
	public bool completed;			// duh
	
	public Objective(string shortName, string displayText){
		this.shortName = shortName;
		this.displayText = displayText;
	}
	
	public string getDisplay(){		// only return text if the quest isn't completed
		if (!completed)
			return displayText;
		else 
			return null;
	}
	
	public void completeObjective(){
		this.completed = true;
	}
}

// derived form of an objective, contains any number of sub quest points
class MultiObjective : Objective {
	public ArrayList bullets = new ArrayList();		// list of all sub-objectives to complete quest
	public MultiObjective(string idName, string description, string[] subobjectives) : base(idName, description){ // call superclass constructor
		for (int i = 0; i < subobjectives.Length; i +=2) {								// convert list of strings into list of objectives, pair by pair
			bullets.Add (new Objective (subobjectives [i], subobjectives [i + 1]));
		}
	}
	
	// the way to properly display a multi-objective quest
	new public string getDisplay(){
		if (!completed) {										// only return text if the objective is not completed
			string text = displayText;							// after adding overarching quest name
			foreach(Objective item in bullets){	
				if(!item.completed)								// if sub objective isn't completed
					//text += "\n\t• " + item.getDisplay();
					text += "\n::::" + item.getDisplay();		// add it's text on a new line after the last point
			}
			return text;
		} else 
			return null;
	}
	
	// called when trying to complete an sub objective of this main quest
	public void completeSubObjective(string name){
		bool alldone = true;
		foreach(Objective item in bullets){
			if(item.shortName.Equals(name))
				item.completeObjective();
			
			if(!item.completed) alldone = false;	// if there are any sub quests not completed, the quest is not complete
		}
		if (alldone)						// if all sub objectives have been completed
			completeObjective();			// mark the whole quest as completed
	}
}