﻿using UnityEngine;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour {

	public bool offlineMode = true;
	bool connecting = false;

	// Use this for initialization
	void Start () {
		PhotonNetwork.player.name = PlayerPrefs.GetString("Username", "Awesome Dude");
	}
	
	void OnDestroy() {
		PlayerPrefs.SetString("Username", PhotonNetwork.player.name);
	}

	void Connect() {
		PhotonNetwork.ConnectUsingSettings( "unity-rpg V1.0" );
	}
	
	void OnGUI() {
		GUI.color = Color.yellow;
		GUILayout.Label( PhotonNetwork.connectionStateDetailed.ToString() );

		UnityEngine.UI.Button startButton = gameObject.GetComponentInChildren<UnityEngine.UI.Button> ();
		if (PhotonNetwork.connected == false && connecting == false )
			PhotonNetwork.player.name = gameObject.GetComponentInChildren<UnityEngine.UI.InputField>().text;
	}
	
	void OnJoinedLobby() {
		Debug.Log ("OnJoinedLobby");

		// Options for the room isVisibile means is it a joinable room or not, max players is well max players
		RoomOptions roomOptions = new RoomOptions() { isVisible = true, maxPlayers = 4 };

		// Makes a room if it is not created with options
		PhotonNetwork.JoinOrCreateRoom(GameObject.FindObjectOfType<LevelLoader> ().levelname, roomOptions, TypedLobby.Default);
	}

	void OnPhotonJoinFailed() {
		Debug.Log ("OnPhotonJoinFailed");
	}

	public void StartGame() {
		offlineMode = gameObject.GetComponentInChildren<UnityEngine.UI.Toggle> ().isOn;
		if (offlineMode) {
			connecting = true;
			PhotonNetwork.offlineMode = true;
			OnJoinedLobby ();
		} else {
			connecting = true;
			Connect ();
		}
	}
	
	void OnJoinedRoom() {
		Debug.Log ("OnJoinedRoom");
		
		connecting = false;
		SpawnMyPlayer();
	}
	
	void SpawnMyPlayer() {
		GameObject myPlayerGO = (GameObject)PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity, 0);
		myPlayerGO.name = PhotonNetwork.player.name;

		((MonoBehaviour)myPlayerGO.GetComponent("MouseLook")).enabled = true;
		((MonoBehaviour)myPlayerGO.GetComponent("PlayerMovement")).enabled = true;

		Vector3 randomVector = new Vector3 (Random.value, Random.value, Random.value);
		myPlayerGO.GetComponent<PhotonView> ().RPC ("SetColor", PhotonTargets.AllBuffered, randomVector);

		myPlayerGO.transform.FindChild("Main Camera").gameObject.SetActive(true);
	}
}
